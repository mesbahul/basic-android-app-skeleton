package com.hobby.mesba.baseapp.dagger;

import android.content.Context;

import com.hobby.mesba.baseapp.backend.BackendApi;
import com.hobby.mesba.baseapp.dagger.modules.AppModule;
import com.hobby.mesba.baseapp.dagger.modules.BackendModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

@Singleton
@Component(modules = {AppModule.class, BackendModule.class})
public interface AppComponent {
    Context context();

    BackendApi api();
}
