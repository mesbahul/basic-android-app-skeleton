package com.hobby.mesba.baseapp.backend;

import com.hobby.mesba.baseapp.models.Post;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Mesbahul Islam on 7/20/2017.
 */

public class BackendClient {
    private final BackendApi api;

    @Inject
    public BackendClient(BackendApi api) {
        this.api = api;
    }

    public Observable<List<Post>> getAllPosts() {
        return api.getPostList();
    }
}
