package com.hobby.mesba.baseapp;

import android.app.Application;

import com.hobby.mesba.baseapp.dagger.AppComponent;
import com.hobby.mesba.baseapp.dagger.DaggerAppComponent;
import com.hobby.mesba.baseapp.dagger.modules.AppModule;
import com.hobby.mesba.baseapp.dagger.modules.BackendModule;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public class BaseApp extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .backendModule(new BackendModule("http://jsonplaceholder.typicode.com/"))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
