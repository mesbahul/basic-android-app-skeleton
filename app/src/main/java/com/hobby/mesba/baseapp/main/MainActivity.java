package com.hobby.mesba.baseapp.main;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hobby.mesba.baseapp.BaseActivity;
import com.hobby.mesba.baseapp.R;
import com.hobby.mesba.baseapp.dagger.AppComponent;
import com.hobby.mesba.baseapp.dagger.CustomScope;
import com.hobby.mesba.baseapp.models.Post;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.Component;
import dagger.Module;
import dagger.Provides;

public class MainActivity extends BaseActivity implements MainActivityContract.View {

    @BindView(R.id.textView_display)
    TextView displayText;
    @BindView(R.id.button_fetch)
    Button fetchButton;

    @Inject
    MainActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerMainActivity_MainActivityComponent.builder()
                .appComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build().inject(this);
    }

    @Override
    public void showPosts(List<Post> posts) {
        displayText.setText("Number of Posts " + posts.size());
    }

    @Override
    public void showError(String message) {
        //Show error message Toast
        Toast.makeText(getApplicationContext(), "Error " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showComplete() {
        //Show completed message Toast
        Toast.makeText(getApplicationContext(), "Complete", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_fetch)
    void onFetchButtonClick() {
        presenter.loadPost();
    }


    @CustomScope
    @Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
    public interface MainActivityComponent {
        void inject(MainActivity activity);
    }

    @Module
    public static class MainActivityModule {
        private final MainActivityContract.View view;

        public MainActivityModule(MainActivityContract.View view) {
            this.view = view;
        }

        @Provides
        MainActivityContract.View provideMainActivityContractView() {
            return view;
        }
    }
}
