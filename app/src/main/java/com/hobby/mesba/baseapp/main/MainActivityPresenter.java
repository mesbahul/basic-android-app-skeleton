package com.hobby.mesba.baseapp.main;

import android.content.Context;

import com.hobby.mesba.baseapp.backend.BackendClient;
import com.hobby.mesba.baseapp.models.Post;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private final Context context;
    private final MainActivityContract.View view;
    private final BackendClient client;

    @Inject
    public MainActivityPresenter(Context context, MainActivityContract.View view, BackendClient client) {
        this.context = context;
        this.view = view;
        this.client = client;
    }

    @Override
    public void loadPost() {
        client.getAllPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Post>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<Post> posts) {
                        view.showPosts(posts);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        view.showComplete();
                    }
                });
    }
}
