package com.hobby.mesba.baseapp.main;

import com.hobby.mesba.baseapp.models.Post;

import java.util.List;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public interface MainActivityContract {
    interface View {
        void showPosts(List<Post> posts);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadPost();
    }
}
