package com.hobby.mesba.baseapp.backend;

import com.hobby.mesba.baseapp.models.Post;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public interface BackendApi {
    @GET("/posts")
    Observable<List<Post>> getPostList();
}
